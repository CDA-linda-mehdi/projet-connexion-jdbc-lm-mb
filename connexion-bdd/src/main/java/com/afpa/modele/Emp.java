package com.afpa.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class Emp {
	private int noemp;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;	
	@NonNull
	private String emploi;
	@NonNull
	private int sup;
	@NonNull
	private String embauche;
	@NonNull
	private float sal;
	@NonNull
	private float comm;
	@NonNull
	private int noserv;
}
