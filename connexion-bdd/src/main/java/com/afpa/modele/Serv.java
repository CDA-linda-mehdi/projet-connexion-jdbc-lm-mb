package com.afpa.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class Serv {
	private int noserv;
	@NonNull
	private String service;
	@NonNull
	private String ville;
}
