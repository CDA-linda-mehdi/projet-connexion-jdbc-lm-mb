package com.afpa.connexion_bdd;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public class MyConnection {

	private static Connection connexion = null;

	private MyConnection() {
		DataSource dataSource = MyDataSourceFactory.getPostgreSQLDataSource();
		try {
			connexion = dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized static Connection getConnection() {
		if (connexion == null) {
			new MyConnection();
		}
		return connexion;
	}

	public static void stop() {
		if (connexion != null) {
			try {
				connexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
