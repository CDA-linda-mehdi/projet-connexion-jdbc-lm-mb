package com.afpa.connexion_bdd;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.postgresql.ds.PGPoolingDataSource;


//import com.mysql.cj.jdbc.MysqlDataSource;

public class MyDataSourceFactory {
	
	public static DataSource getPostgreSQLDataSource() {
		Properties props = new Properties();
		FileInputStream fis = null;
		//MysqlDataSource mysqlDataSource = null;
		PGPoolingDataSource source = null;
		try {
			fis = new FileInputStream("conf/db.properties");
			props.load(fis);
			source = new PGPoolingDataSource();
			source.setServerName(props.getProperty("url"));
			source.setUser(props.getProperty("username"));
			source.setPassword(props.getProperty("password"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return source;
	}
}
