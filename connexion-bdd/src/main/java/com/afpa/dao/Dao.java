package com.afpa.dao;

import java.util.List;

public interface Dao<T> {
	T save(T obj);
	T update(T obj);
	T findById(int id);
	List<T> getAll();
}
