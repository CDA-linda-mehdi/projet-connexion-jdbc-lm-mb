package com.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.afpa.connexion_bdd.MyConnection;
import com.afpa.modele.Emp;



public class EmpBddDaoImpl implements Dao {


		//met à jour les informations d'une Emp
		public Emp update(Emp emp) {
			// TODO Auto-generated method stub
			return null;
		}

		//liste les Emps
		public List<Emp> getAll() {
			Connection c = MyConnection.getConnection();
			List<Emp> res = null;
			try {
				PreparedStatement selectAllPs = c.prepareStatement("SELECT * FROM emp;");
				ResultSet result = selectAllPs.executeQuery();
				res = new ArrayList<Emp>();
				while (result.next()) {
					// on indique chaque fois le nom de la colonne et le type
					int noemp = result.getInt("noemp");

					String nom = result.getString("nom");

					String prenom = result.getString("prenom");
					
					String emploi = result.getString("emploi");

					int sup = result.getInt("sup");

					String embauche = result.getString("embauche");
					
					float sal = result.getFloat("sal");

					float comm = result.getFloat("comm");

					int noserv = result.getInt("noserv");

					// pareil pour tous les autres attributs
					Emp p = new Emp(noemp, nom, prenom, emploi, sup, embauche, sal, comm, noserv);
					p.setNoemp(noemp);
					p.setNom(nom);
					p.setPrenom(prenom);
					p.setEmploi(emploi);
					p.setSup(sup);
					p.setEmbauche(embauche);
					p.setSal(sal);
					p.setComm(comm);
					p.setNoserv(noserv);
					res.add(p);
				}
				return res;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return null;
		}

		//ajoute une Emp
		public Emp save(Emp emp) {
			Connection c = MyConnection.getConnection();
			if (c != null) {
				try {
					PreparedStatement ps = c.prepareStatement("insert into emp (noemp,nom,prenom,emploi,sup,embauche,sal,comm,noserv) values (?,?,?,?,?,?,?,?,?); ", PreparedStatement.RETURN_GENERATED_KEYS);
					ps.setInt(1, emp.getNoemp()); //1 pour le numéro de colonne
					ps.setString(2, emp.getNom());
					ps.setString(3, emp.getPrenom());
					ps.setString(4, emp.getEmploi());
					ps.setInt(5, emp.getSup());
					ps.setString(6, emp.getEmbauche());
					ps.setFloat(7, emp.getSal());
					ps.setFloat(8, emp.getComm());
					ps.setInt(9, emp.getNoserv());
					ps.executeUpdate();
					ResultSet resultat = ps.getGeneratedKeys();
					if (resultat.next()) {
						emp.setNoemp(resultat.getInt("noemp"));//on insére un nouvel employé à partir de la nouvelle incrémentation de l'ID
						emp.setNom(resultat.getString("nom"));
						emp.setPrenom(resultat.getString("prenom"));
						emp.setEmploi(resultat.getString("emploi"));
						emp.setSup(resultat.getInt("sup"));
						emp.setEmbauche(resultat.getString("embauche"));
						emp.setSal(resultat.getFloat("sal"));
						emp.setComm(resultat.getFloat("comm"));
						emp.setNoserv(resultat.getInt("noserv"));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return emp;
		}

		//trouve une Emp à partir de son ID
		public Emp findById(int id) {
			Connection c = MyConnection.getConnection();
			if (c != null) {
				try {
					PreparedStatement ps = c.prepareStatement("select * from emp where num = ?; ");
					ps.setInt(1, id);
					ResultSet r =ps.executeQuery();
					if (r.next())
						return new Emp(r.getInt("noemp"),r.getString("nom"),r.getString("prenom"),r.getString("emploi"),r.getInt("sup"),r.getString("embauche"),r.getFloat("sal"),r.getFloat("comm"),r.getInt("noserv"));
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		public Object save(Object obj) {
			// TODO Auto-generated method stub
			return null;
		}

		public Object update(Object obj) {
			// TODO Auto-generated method stub
			return null;
		}

}
