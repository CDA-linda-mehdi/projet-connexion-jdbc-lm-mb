package com.afpa.exec;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import com.afpa.connexion_bdd.MyConnection;
import com.afpa.dao.EmpBddDaoImpl;
import com.afpa.modele.Emp;


public class Main {

	private static Connection connection;
	private static boolean authentifie = true;
	private static PreparedStatement insertionPS;
	private static PreparedStatement selectAllPs;
	private static PreparedStatement loginPs;
	
	private static void initConnection() throws Exception {
//		String url = "jdbc:postgresql://localhost:3452/projet-jdbc";
//		String user = "postgres";
//		String password = "Linda59*";
//		connection = DriverManager.getConnection(url, user, password);
		
//		connection = MyConnection.getConnection();
//		Properties props = new Properties();
//		FileInputStream fis = new FileInputStream("conf/db.properties");
//		props.load(fis);
//		connection = DriverManager.getConnection(props.getProperty("url"), props.getProperty("user"), props.getProperty("password")); 
//		
		connection.setAutoCommit(false);
		
		insertionPS = connection.prepareStatement("INSERT INTO emp (noemp,nom,prenom, emploi, sup, embauche, sal, comm, noserv)	VALUES (?,?,?,?,?,?,?,?,?);",PreparedStatement.RETURN_GENERATED_KEYS);
		selectAllPs = connection.prepareStatement("SELECT * FROM emp;");
		loginPs = connection.prepareStatement("select * from emp where nom = ? ;");
	}

	private static void ajouterUnEmp(Scanner sc) throws Exception {
		if (!authentifie) {
			System.out.println("veuillez d'abord vous authentifier !!! ");
			System.out.println("veuillez vous loguer.");
			return;
		}
		System.out.println("ajout emp : ");
		System.out.print(" saisir numéro employé >");
		int noemp = sc.nextInt();
		sc.nextLine();
		System.out.print(" saisir nom >");
		String nom = sc.nextLine();
		System.out.print(" saisir prenom >");
		String prenom = sc.nextLine();
		System.out.print(" saisir emploi >");
		String emploi = sc.nextLine();
		System.out.print(" saisir supérieur hiérarchique >");
		int sup = sc.nextInt();
		sc.nextLine();
		System.out.print(" saisir date embauche >");
		String embauche = sc.nextLine();
		System.out.print(" saisir salaire >");
		float sal = sc.nextFloat();
		sc.nextLine();
		System.out.print(" saisir commission >");
		float comm = sc.nextFloat();
		sc.nextLine();
		System.out.print(" saisir numéro de service >");
		int noserv = sc.nextInt();
		sc.nextLine();
		
		insertionPS.setInt(1, noemp);
		insertionPS.setString(2, nom);
		insertionPS.setString(3, prenom);
		insertionPS.setString(4, emploi);
		insertionPS.setInt(5, sup);
		insertionPS.setString(6, embauche);
		insertionPS.setFloat(7, sal);
		insertionPS.setFloat(8, comm);
		insertionPS.setInt(9, noserv);
		
		int nbr = insertionPS.executeUpdate();
		if (0 != nbr)
			System.out.println("insertion réussie");
	}

	private static void afficherPersonnes() throws Exception {
		if (!authentifie) {
			System.out.println("veuillez d'abord vous authentifier !!! ");
			System.out.println("veuillez vous loguer.");
			return;
		}
		ResultSet result = selectAllPs.executeQuery();
		while (result.next()) {
			// on indique chaque fois le nom de la colonne et le type
			int idPersonne = result.getInt("num");

			String nom = result.getString("nom");

			String prenom = result.getString("prenom");

			// pareil pour tous les autres attributs
			System.out.println(idPersonne + " " + nom + " " + prenom);
		}
		System.out.println();
	}

	public static void main(String[] args) throws Exception {
		
		final List<Integer> actionsPrivees = Arrays.asList(0,1,2,4,5);
		final List<Integer> actionsPubliques = Arrays.asList(0,3);
		
		try {
			//initConnection();

			Scanner sc = new Scanner(System.in);

			int choix = 0;
			boolean continuer = true;

			while (continuer) {
				System.out.println();
				
				if(authentifie) {
					System.out.println("0- arrêter le programme");
					System.out.println("1- ajouter un employé");
					System.out.println("2- modifier le salaire d'un employé");
					System.out.println("4- ajouter un service");
					System.out.println("5- lister les employés d'un service (le numero est à saisir)");
					System.out.println("6- lister les services avec le nombre d'employés par service");
					System.out.println("7- afficher le plus haut salaire pour chaque service");
					System.out.println("8- afficher le plus bas salaire pour chaque service.");
				} else {
					System.out.println("0- arrêter le programme");
					System.out.println("3- login");
				}
				
				System.out.print("> ");

				choix = sc.nextInt();
				sc.nextLine();

				if( (authentifie && !actionsPrivees.contains(choix))
					|| (!authentifie && !actionsPubliques.contains(choix)) ) {
					choix = -1;
				}
				
				switch (choix) {
				case 0:
					System.out.println("au revoir !");
					continuer = false;
					break;
				case 1:
					System.out.println("1- ajouter un employé : ");
					ajouterUnEmp(sc);
					EmpBddDaoImpl emp = new EmpBddDaoImpl();
					emp.save(emp);
					break;
				case 2:
					System.out.println("2- modifier le salaire d'un employé");
					afficherPersonnes();
					break;
				case 3:
					System.out.println("3- login");
					break;
				case 4:
					System.out.println("4- ajouter un service");
					break;
				case 5:
					System.out.println("5- lister les employés d'un service (le numero est à saisir)");
					break;
				case 6 :
					System.out.println("6- lister les services avec le nombre d'employés par service");
					break;
				case 7:
					System.out.println("7- afficher le plus haut salaire pour chaque service");
					break;
				case 8:
					System.out.println("8- afficher le plus bas salaire pour chaque service.");
					break;
				default:
					System.out.println("action invalide !");
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	private static void annulerModifications() throws Exception {
		connection.rollback();
	}

	private static void validerModifications() throws Exception {
		connection.commit();
	}

	private static void login(Scanner sc) throws Exception {
		System.out.println("entrez votre nom : ");
		System.out.print(">");
		String nom = sc.nextLine();

		loginPs.setString(1, nom);
		
		ResultSet result = loginPs.executeQuery();
		
		System.out.println(loginPs);
		
		if (result.next()) {
			authentifie = true;
			System.out.println("auth reussie !");
		} else {
			System.out.println("mauvais login mot de passe !!");
		}
	}

}

